<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>PR005 - Listas de elementos</title>

        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <div class="contenido">
            <div class="lista1">
                <select name="lista1" id="lista1" multiple>
                    <option>[L1] Item 1</option>
                    <option>[L1] Item 2</option>
                    <option>[L1] Item 3</option>
                    <option>[L1] Item 4</option>
                    <option>[L1] Item 5</option>
                </select>
            </div>
            <div class="botones">
                <p>
                    <button id="one-left"><</button>
                </p>
                <p>
                    <button id="one-right">></button>
                </p>
                <p>
                    <button id="all-left">&laquo;</button>
                </p>
                <p>
                    <button id="all-right">&raquo;</button>
                </p>
            </div>
            <div class="lista2">
                <select name="lista2" id="lista2" multiple>
                    <option>[L2] Item 1</option>
                    <option>[L2] Item 2</option>
                    <option>[L2] Item 3</option>
                    <option>[L2] Item 4</option>
                    <option>[L2] Item 5</option>
                </select>
            </div>
        </div>

        <div class="contenido">
            <div class="nuevo-elemento">
                <p><input type="text" name="nuevo-elemento" id="nuevo-elemento"/></p>
                <p><input type="submit" value="Crear nuevo elemento" id="submit-nuevo-elemento"/></p>
            </div>
        </div>

        <script src="js/javascript.js"></script>
    </body>
</html>
