var lista1 = document.getElementById('lista1');
var lista2 = document.getElementById('lista2');
var campoNuevoElemento = document.getElementById('nuevo-elemento');


/* Movemos los elementos seleccionados de una lista a la otra */
document.getElementById('one-left').onclick = function () {
    for (var i = lista2.options.length-1; i != -1; i--) {
        if (lista2.item(i).selected) {
            lista1.appendChild(lista2.item(i));
        }
    }
};

document.getElementById('one-right').onclick = function () {
    for (var i = lista1.options.length-1; i != -1; i--) {
        if (lista1.item(i).selected) {
            lista2.appendChild(lista1.item(i));
        }
    }
};
/**************************************************************/


/* Movemos todos los elementos de una lista a la otra */
document.getElementById('all-left').onclick = function () {
    for (var i = lista2.options.length-1; i != -1; i--) {
        lista1.appendChild(lista2.item(i));
    }
};

document.getElementById('all-right').onclick = function () {
    for (var i = lista1.options.length-1; i != -1; i--) {
        lista2.appendChild(lista1.item(i));
    }
};
/******************************************************/


document.getElementById('submit-nuevo-elemento').onclick = function () {
    // Creamos el nuevo elemento
    var nuevoElemento = document.createElement('option');

    // Le añadimos el texto agregado por el usuario en el campo de texto
    nuevoElemento.appendChild(document.createTextNode(campoNuevoElemento.value));

    // Añadimos el nuevo elemento a la primera lista
    lista1.appendChild(nuevoElemento);

    // Limpiamos el campo de texto
    campoNuevoElemento.value = "";
};
