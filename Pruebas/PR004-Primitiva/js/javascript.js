// Función que genera un Array con los números de la primitiva
function getNumerosPrimitiva() {
    // Creamos las variables
    var numerosPrimitiva = [];

    // Creamos los números
    for (var i = 0; i < 6; i++) {
        // Generamos el número aleatorio
        do {
            // Reiniciamos el contador
            var contador = 0;

            // Generamos el número aleatorio
            numero = Math.floor(Math.random() * 49) + 1;

            // Recorremos el array para comprobar si el número ya está en el array
            numerosPrimitiva.forEach(function (numeroArray) {
                if (numero == numeroArray) {
                    contador++;
                }
            });

            // Si el número ya se enconbtraba en el Array, volvemos a generarlo
        } while (contador > 0)

        // Finalmente introducimos el número en el Array
        numerosPrimitiva.push(numero);
    }

    // Devolvemos el Array de números
    return numerosPrimitiva;
}

// Función que muestra por pantalla los números
function viewNumerosPrimitiva() {
    var numeros = "";

    // Recorremos el Array de números
    getNumerosPrimitiva().forEach(function (numeroPrimitiva) {
        // Guardamos mlos números en un String
        numeros = numeros + " " + numeroPrimitiva;

    });

    document.getElementById("numeros").innerHTML = "<br />Los números de la primitiva son: " + numeros;
}

