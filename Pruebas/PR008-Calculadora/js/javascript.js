window.onload = function () {
    // Declaramos una variable con el Array de teclas
    var teclas = document.getElementsByClassName('tecla');

    // Declaramos una variable con la pantalla
    var pantalla = document.getElementById('pantalla');

    // Declaramos una variable vacia donde se guardarán los números y sus operaciones
    var operaciones = '';

    // Recorremos el Array de teclas para asignarle el evento Click a cada una
    for (var i = 0; i < teclas.length; i++) {
        teclas.item(i).addEventListener('click', function (e) {

            if (this.id === 'tecla-limpiar') { // Si la tecla es la de limpiar, reiniciamos la variable de operaciones y ponemos la pantalla a 0
                operaciones = '';
                pantalla.textContent = 0;
            } else {
                var errores = 0;

                // Si la tecla es de operación o punto, añadimos el símbolo correspondiente a la variable de operaciones
                if (this.id === 'tecla-sumar') {
                    operaciones += '+';
                } else if (this.id === 'tecla-restar') {
                    operaciones += '-';
                } else if (this.id === 'tecla-multiplicar') {
                    operaciones += '*';
                } else if (this.id === 'tecla-dividir') {
                    operaciones += '/';
                } else if (this.id === 'tecla-punto') {
                    operaciones += '.';
                } else if (this.id === 'tecla-resultado') {
                    if ((op = eval(operaciones)) && !isNaN(operaciones.substr(operaciones.length - 1))) {
                        operaciones = op;
                    } else {
                        errores++;
                    }
                } else { // Si no es ninguna de operación o puunto, es un número, así que agregamos el número correspondiente a la variable
                    operaciones += this.id.split('-')[1];
                }

                if (0 === errores) { // Si no hay errores, calculamos las operaciones
                    pantalla.textContent = operaciones;

                    pantalla.scrollLeft = screen.width;
                } else { // Si hay algún error, avisamos al usuario sin calcular las operaciones
                    alert('Ha ocurrido un error. ¿Ha introducido una operación válida?');
                }
            }
        });
    }

    // Añadimos un listener para el teclado
    document.addEventListener('keyup', function (e) {
        if (e.keyCode >= 48 && e.keyCode <= 57) { // Números del teclado alfanumérico
            document.getElementById('tecla-' + (e.keyCode-48)).click();
        } else if (e.keyCode >= 96 && e.keyCode <= 105) { // Números del teclado numérico
            document.getElementById('tecla-' + (e.keyCode-96)).click();
        } else if (e.keyCode === 110 || e.keyCode === 190) { // Punto de los teclados alfanumérico y númerico
            document.getElementById('tecla-punto').click();
        } else if (e.keyCode === 111) { // Barra (/)
            document.getElementById('tecla-dividir').click();
        } else if (e.keyCode === 106) { // Asterisco (*)
            document.getElementById('tecla-multiplicar').click();
        } else if (e.keyCode === 109) { // Guión (-)
            document.getElementById('tecla-restar').click();
        } else if (e.keyCode === 107) { // Cruz (+)
            document.getElementById('tecla-sumar').click();
        } else if (e.keyCode === 13) { // Enter/Intro
            document.getElementById('tecla-resultado').click();
        } else if (e.keyCode === 46) { // Supr
            document.getElementById('tecla-limpiar').click();
        }
    })
};
