// Creamos una función a la que le pasamos una cadena de texto
// Devuelve una cadena de texto que dice si la cadena es palíndroma
function textoPalindromo(texto) {
    // Pasamos el texto a minúsculas
    texto = texto.toLowerCase();

    // Dividimos el texto en un array de letras
    textoArray = texto.split("");

    // Limpiamos "texto"
    texto = "";

    // Recorremos el array eliminando los espacios 
    for (letraKey in textoArray) {
        if (textoArray[letraKey] !== " ") {
            texto += textoArray[letraKey];
        }
    }

    // Volvemos a dividir el texto en un array, y creamos una versión revertida
    textoArray = texto.split("");
    textoArrayRevertido = texto.split("").reverse();

    // Recorremos ambos arrays comparando sus letras.
    // Si alguna letra no coincide, devolvemos que la cadena no es palíndroma
    for (letraKey in textoArray) {
        if (textoArray[letraKey] !== textoArrayRevertido[letraKey]) {
            return "La cadena no es palíndroma";
        }
    }

    // Si finalizamos el bucle sin haber entrado en el if, devolvemos que la cadena si es palíndroma
    return "La cadena es palíndroma.";
}

// Mostramos un alert con el resultado de la función
alert(textoPalindromo("La ruta nos aporto otro paso natural"));
