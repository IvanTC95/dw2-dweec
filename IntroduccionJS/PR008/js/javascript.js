// Creamos una función a la que le pasamos un número entero
// Devuelve una cadena de texto que dice si el número es par o impar
function parImpar(numero) {
    if (numero % 2 == 0) {
        return "El número es par";
    } else {
        return "El número es impar";
    }
}

alert(parImpar(0));