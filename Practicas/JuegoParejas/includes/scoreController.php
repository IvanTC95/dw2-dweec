<?php
$con = mysqli_connect('localhost', 'root', '', 'dweec_parejas');

if (isset($_GET['action']) && $_GET['action'] === 'submit') {
    $key = 'SomethingStrangeBuff4235';

    $data                = base64_decode($_POST['secretCode']);
    $iv                  = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));
    $codedDateTimeString = rtrim(
        mcrypt_decrypt(
            MCRYPT_RIJNDAEL_128,
            hash('sha256', $key, true),
            substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
            MCRYPT_MODE_CBC,
            $iv
        ),
        "\0"
    );

    $actualDatetimeLessMinutes = new DateTime();
    $actualDatetimeLessMinutes->sub(date_interval_create_from_date_string('20 minutes'));
    $actualDatetimeLessMinutes = DateTime::createFromFormat('Y-m-d h:i:s', $actualDatetimeLessMinutes->format('Y-m-d h:i:s'));
    $codedDateTime             = DateTime::createFromFormat('Y-m-d h:i:s', $codedDateTimeString);

    if ($codedDateTime > $actualDatetimeLessMinutes && $codedDateTime < new DateTime()) {
        mysqli_query($con, 'INSERT INTO couples_ranking(`user`, `score`, `time`, `date`) VALUES ("' . $_POST['user'] . '", ' . $_POST['score'] . ', ' . $_POST['time'] . ', "' . (new \DateTime())->format('Y-m-d') . '")');
    }
}

$result = mysqli_query($con, 'SELECT r1.id, r1.user, r1.score, MIN(r1.time) AS minTime FROM couples_ranking r1 JOIN (SELECT user, MIN(score) AS minScore FROM couples_ranking GROUP BY user) r2 ON r1.user = r2.user AND r1.score = r2.minScore GROUP BY user ORDER BY r1.score ASC, minTime ASC;');

echo json_encode(mysqli_fetch_all($result));
