<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Juego de las parejas</title>

        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <?php

        $key = 'SomethingStrangeBuff4235';
        $string = (new DateTime())->format('Y-m-d h:i:s');

        $iv = mcrypt_create_iv(
            mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
            MCRYPT_DEV_URANDOM
        );

        $encrypted = base64_encode(
            $iv .
            mcrypt_encrypt(
                MCRYPT_RIJNDAEL_128,
                hash('sha256', $key, true),
                $string,
                MCRYPT_MODE_CBC,
                $iv
            )
        );

        ?>
        <div id="protection-screen" style="display: none;"></div>

        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/md5.js"></script>
        <script src="js/Game.min.js"></script>
        <script src="js/Card.min.js"></script>
        <script>
            $(function () {
                (new Game('<?php echo $encrypted;?>')).drawBoard();
            });
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-15069154-26"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-15069154-26');
        </script>
    </body>
</html>
