class Game {
    constructor(secretCode) {
        this.cards = [];
        this.rotatedCards = [];
        this.movements = 0;
        this.seconds = 0;
        this.secretCode = secretCode;

        for (let i = 0; i < 8; i++) {
            for (let o = 1; o < 3; o++) {
                this.cards.push(new Card(i + 1, o, this));
            }
        }

        this.randomizeCards();
    }

    randomizeCards() {
        let ctr = this.cards.length, temp, index;

        while (ctr > 0) {
            index = Math.floor(Math.random() * ctr);
            ctr--;
            temp = this.cards[ctr];
            this.cards[ctr] = this.cards[index];
            this.cards[index] = temp;
        }
    }

    addRotatedCard(card) {
        let that = this;

        if (this.rotatedCards.length % 2 === 0) {
            this.rotatedCards.push(card);
        } else {
            if (this.rotatedCards[this.rotatedCards.length - 1].getId() === card.getId()) {
                this.rotatedCards.push(card);
            } else {
                setTimeout(
                    function () {
                        card.rotate(true);
                        that.rotatedCards.pop().rotate(true);
                    }, 500);
            }
        }


        if (this.rotatedCards.length === 16) {
            setTimeout(
                function () {
                    that.drawBoard(true);
                }, 1000);
        }
    }

    drawBoard(winner = false) {
        let that = this;
        $('body .board').remove();

        let boardDiv = $('<div class="board"></div>');

        if (!winner) {
            for (let row = 0; row < 4; row++) {
                let rowDiv = $('<div class="row"></div>');

                for (let cell = 0; cell < 4; cell++) {
                    let cellDiv = $('<div class="cell"></div>');
                    cellDiv.append(this.cards[row * 4 + cell].getNode());

                    rowDiv.append(cellDiv);
                }

                boardDiv.append(rowDiv);
            }
        } else {
            this.stopChronometer();
            let winnerP = $('<p class="winner">¡¡ENHORABUENA!!<br />¡¡HAS GANADO!!<br /></p>');

            winnerP.append('<span class="small">Movimientos: ' + this.movements + '</span><br />');
            winnerP.append('<span class="small">Tiempo: ' + this.seconds + ' segundos</span><br />');

            let divWinnerLetter = $('<div></div>');

            for (let i = 0; i < 3; i++) {
                let selectWinnerLetter = $('<select id="selectWinnerLetter-' + (i + 1) + '" class="winnerLetter"></select>');

                for (let o = 0; o < 26; o++) {
                    let selectWinnerLetterOption = $('<option value="' + String.fromCharCode(65 + o) + '">' + String.fromCharCode(65 + o) + '</option>');

                    selectWinnerLetter.append(selectWinnerLetterOption);
                }

                divWinnerLetter.append(selectWinnerLetter);
            }

            let submitWinner = $('<input type="submit" value="Enviar puntuación" id="sendScore" />');

            submitWinner.click(
                function () {
                    Game.sendScore($('#selectWinnerLetter-1').val() + $('#selectWinnerLetter-2').val() + $('#selectWinnerLetter-3').val(), that.movements, that.seconds, that.secretCode);

                    divWinnerLetter.empty();

                    divWinnerLetter.append('<span class="small">Tu puntuación ha sido enviada correctamente.</span>');
                }
            );

            divWinnerLetter.append(submitWinner);

            winnerP.append(divWinnerLetter);

            let reloadP = $('<br /><br /><span class="reload">Reiniciar el juego</span>');

            reloadP.click(function () {
                location.reload();
            });

            winnerP.append(reloadP);

            boardDiv.append(winnerP);
        }

        $('body').append(boardDiv);

        Game.drawRanking();
    }

    startChronometer() {
        let that = this;

        this.chronometer = setInterval(function () {
            that.seconds++;
        }, 1000);

    }

    stopChronometer() {
        clearInterval(this.chronometer);
    }

    static sendScore(user, score, time, secretCode) {
        $.post(
            "./includes/scoreController.php?action=submit",
            {
                user: user,
                score: score,
                time: time,
                secretCode: secretCode
            }
        )
            .done(
                function (data) {
                    $('#ranking').remove();

                    let rankingDiv = $('<div id="ranking"><div class="title">Ranking</div></div>');
                    let contentDiv = $('<div class="content"><div class="line"><div class="code">Usuario</div><div class="score">Punt.</div><div class="time">Tiem.</div></div></div>');

                    rankingDiv.append(contentDiv);

                    let scores = $.parseJSON(data);

                    scores.forEach(
                        function (score) {
                            let lineDiv = $('<div class="line"><div class="code">' + score[1] + '</div><div class="score">' + score[2] + '</div><div class="time">' + score[3] + '</div></div>');

                            contentDiv.append(lineDiv);
                        }
                    );

                    $('body').append(rankingDiv);
                }
            );
    }

    static drawRanking() {
        $.ajax({
            url: "./includes/scoreController.php"
        })
            .done(
                function (data) {
                    $('#ranking').remove();

                    let rankingDiv = $('<div id="ranking"><div class="title">Ranking</div></div>');
                    let contentDiv = $('<div class="content"><div class="line"><div class="code">Usuario</div><div class="score">Punt.</div><div class="time">Tiem.</div></div></div>');

                    rankingDiv.append(contentDiv);

                    let scores = $.parseJSON(data);

                    if (scores.length > 0) {
                        scores.forEach(
                            function (score) {
                                let lineDiv = $('<div class="line"><div class="code">' + score[1] + '</div><div class="score">' + score[2] + '</div><div class="time">' + score[3] + '</div></div>');

                                contentDiv.append(lineDiv);
                            }
                        );
                    } else {
                        contentDiv.append('<span>Aun no hay puntuaciones...</span>');
                    }

                    $('body').append(rankingDiv);
                }
            );
    }
}
