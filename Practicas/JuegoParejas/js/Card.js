class Card {
    constructor(id, couple, board) {
        this.id = id;
        this.couple = couple;
        this.board = board;

        this.img = id + ".png";
        this.rotated = false;
        this.fixed = false;
        this.node = $('<img src="img/back.png" class="card no-rotated" style="width: 100%;" id="'+CryptoJS.MD5("card-" + this.id + "_" + this.couple)+'" />');

    }

    rotate(badCouple = false) {
        if (!this.rotated && !badCouple || badCouple) {
            let that = this;
            this.rotated = !this.rotated;

            let card = $('#' + CryptoJS.MD5("card-" + this.id + "_" + this.couple));
            let protectionScreen = $('#protection-screen');

            if (this.rotated) {
                card.attr('class', 'card');
            } else {
                card.attr('class', 'card no-rotated');
            }

            protectionScreen.css({'display': 'block'});
            setTimeout(
                function () {
                    protectionScreen.css({'display': 'none'});
                }, 300);

            card.css({'transform': 'rotateY(90deg)'});
            setTimeout(
                function () {
                    card.attr('src', 'img/' + (that.rotated ? that.img : 'back.png'));
                }, 200);
            card.css({'transform': 'rotateY(180deg)'});
            card.css({'transform': 'rotateY(' + (this.rotated ? 180 : 0) + 'deg)'});


            if (!badCouple) {
                this.board.addRotatedCard(this);
            }
        }

        if (!badCouple) {
            this.board.movements++;
        }

        if (this.board.movements === 1) {
            this.board.startChronometer();
        }
    }

    getNode() {
        let that = this;

        this.node.click(function () {
            that.rotate();
        });

        return this.node;
    }

    getId() {
        return this.id;
    }
}
