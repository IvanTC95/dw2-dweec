function Coche(capacidadDeposito, avatar) {
    var that = this;

    this.posicion = new Posicion(window.innerWidth/2, window.innerHeight/2);
    this.nivelCombustible = capacidadDeposito;
    this.kilometrosRecorridos = 0;
    this.capacidadDeposito = capacidadDeposito;
    this.avatar = avatar;

    this.kmPorPixel = 5;
    this.consumo100km = 8;
    this.velocidad = 10;

    this.imagenCoche = document.createElement('img');
    this.imagenCoche.src = 'img/' + this.avatar + '.png';
    this.imagenCoche.id = 'coche';
    document.body.appendChild(this.imagenCoche);

    this.imagenCoche.style.transform = 'translate(' + this.posicion.posicionX + 'px, ' + this.posicion.posicionY + 'px) rotateZ(0deg)';

    this.mover = function (direccion) {
        if (that.nivelCombustible - that.consumo100km > 0) {
            var movimiento = false;
            if (direccion === 'N' && that.posicion.posicionY > (that.imagenCoche.clientWidth - that.imagenCoche.clientHeight) / 2) {
                that.imagenCoche.style.transform = that.imagenCoche.style.transform.replace(/rotateZ\([0-9]*deg\)/, 'rotateZ(270deg)');

                that.posicion.modificar(0, -that.velocidad);

                movimiento = true;
            }
            else if (direccion === 'S' && that.posicion.posicionY < (window.innerHeight - (that.imagenCoche.clientHeight + (that.imagenCoche.clientWidth - that.imagenCoche.clientHeight) / 2))) {
                that.imagenCoche.style.transform = that.imagenCoche.style.transform.replace(/rotateZ\([0-9]*deg\)/, 'rotateZ(90deg)');

                that.posicion.modificar(0, +that.velocidad);

                movimiento = true;
            } else if (direccion === 'E' && that.posicion.posicionX < (window.innerWidth - that.imagenCoche.clientWidth)) {
                that.imagenCoche.style.transform = that.imagenCoche.style.transform.replace(/rotateZ\([0-9]*deg\)/, 'rotateZ(0deg)');

                that.posicion.modificar(+that.velocidad, 0);

                movimiento = true;
            } else if (direccion === 'O' && that.posicion.posicionX > 0) {
                that.imagenCoche.style.transform = that.imagenCoche.style.transform.replace(/rotateZ\([0-9]*deg\)/, 'rotateZ(180deg)');

                that.posicion.modificar(-that.velocidad, 0);

                movimiento = true;
            }

            if (movimiento) {
                that.imagenCoche.style.transform = that.imagenCoche.style.transform.replace(/translate\([0-9]*px, [0-9]*px\)/, 'translate(' + that.posicion.posicionX + 'px, ' + that.posicion.posicionY + 'px)');

                that.kilometrosRecorridos += that.kmPorPixel * that.velocidad;

                if (that.kilometrosRecorridos % 100 === 0) {
                    that.nivelCombustible -= that.consumo100km;
                }
            }
        } else {
            alert('Se ha quedado sin combustible, necesita repostar.');
        }
    };

    this.repostar = function (litros) {
        if (-1 === litros || that.nivelCombustible + litros > that.capacidadDeposito || 'undefined' === litros || '' === litros) {
            that.nivelCombustible = that.capacidadDeposito;
        } else {
            that.nivelCombustible += litros;
        }
    };

    this.verConsola = function () {
        var consolawidth = 1000;
        var consolaHeight = 200;
        var consolaLeft = (window.innerWidth - consolawidth) / 2;
        var consolaTop = (window.innerHeight - consolaHeight) / 2;

        var consola = window.open("", "", "left=" + consolaLeft + ",top=" + consolaTop + ",width=" + consolawidth + ",height=" + consolaHeight);

        consola.document.body.innerHTML =
            '<table style="width: 100%;">' +
            '<tr>' +
            '<th style="width: 33%;">Km recorridos::</th>' +
            '<th style="width: 33%;">Combustible restante:</th>' +
            '<th style="width: 33%;">Km que puede recorrer:</th>' +
            '</tr>' +
            '<tr>' +
            '<td style="text-align: center;"><span style="font-weight: bold; font-size: 5em;">' + that.kilometrosRecorridos + '</span></td>' +
            '<td style="text-align: center;"><span style="font-weight: bold; font-size: 5em;">' + that.nivelCombustible + '</span>/' + that.capacidadDeposito + '</td>' +
            '<td style="text-align: center;"><span style="font-weight: bold; font-size: 5em;">' + (that.nivelCombustible < that.consumo100km ? 0 : that.nivelCombustible / that.consumo100km * 100) + '</span></td>' +
            '</tr>' +
            '</table>';

        consola.document.addEventListener('keyup', function (e) {
            if (e.keyCode === 27 || e.keyCode === 86) {
                consola.close();
            }
        });
    };
}

function Posicion(posicionX, posicionY) {
    var that = this;

    this.posicionX = posicionX;
    this.posicionY = posicionY;

    this.modificar = function (modificarX, modificarY) {
        that.posicionX += modificarX;
        that.posicionY += modificarY;
    };
}
